//
//  main.m
//  top
//
//  Created by 西田 匡志 on 2013/09/29.
//  Copyright (c) 2013年 西田 匡志. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "m3_AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([m3_AppDelegate class]));
    }
}
