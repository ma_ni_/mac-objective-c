//
//  m3_AppDelegate.m
//  top
//
//  Created by 西田 匡志 on 2013/09/29.
//  Copyright (c) 2013年 西田 匡志. All rights reserved.
//

#import "m3_AppDelegate.h"
#import "MiddleAppDelegate.h"
#import "AppDelegate.h"

static id ad;

@implementation m3_AppDelegate

- (UIViewController *)startRootViewController
{
    UIViewController *vc = [[UIViewController alloc] init];
    
    UIButton *m_btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    m_btn.frame = CGRectMake(10, 150, 100, 30);
    [m_btn setTitle:@"Middle" forState:UIControlStateNormal];
    [m_btn addTarget:self action:@selector(mbAction:) forControlEvents:UIControlEventTouchDown];
    [vc.view addSubview:m_btn];
    
    UIButton *s_btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    s_btn.frame = CGRectMake(200, 150, 100, 30);
    [s_btn setTitle:@"Spriate" forState:UIControlStateNormal];
    [s_btn addTarget:self action:@selector(sbAction:) forControlEvents:UIControlEventTouchDown];
    [vc.view addSubview:s_btn];
    
    return vc;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [self startRootViewController];
    [self.window.rootViewController.view setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)mbAction:(UIButton *)btn
{
    [self.window.rootViewController presentViewController:[[[MiddleAppDelegate alloc] init] startRootViewController] animated:YES completion:nil];
}

- (void)sbAction:(UIButton *)btn
{
    [self.window.rootViewController presentViewController:[[[AppDelegate alloc] init] startRootViewController] animated:YES completion:nil];
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
