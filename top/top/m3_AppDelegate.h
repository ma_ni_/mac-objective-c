//
//  m3_AppDelegate.h
//  top
//
//  Created by 西田 匡志 on 2013/09/29.
//  Copyright (c) 2013年 西田 匡志. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface m3_AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (UIViewController *)startRootViewController;

@end
