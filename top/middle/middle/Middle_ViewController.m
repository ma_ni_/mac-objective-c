//
//  ViewController.m
//  middle
//
//  Created by 西田 匡志 on 2013/09/29.
//  Copyright (c) 2013年 西田 匡志. All rights reserved.
//

#import "Middle_ViewController.h"

@interface Middle_ViewController ()

@end

@implementation Middle_ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor greenColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backTopProject:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
